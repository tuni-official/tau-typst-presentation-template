# TAU Typst Presentation template

This repository contains a [typst] template for Tampere university
presentations.

[typst]: https://github.com/typst/typst/blob/main/README.md

## License

This template is MIT-licensed. See the file [LICENSE](./LICENSE) for details.
